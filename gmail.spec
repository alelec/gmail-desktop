# -*- mode: python -*-

block_cipher = None

import os
import cefpython3
from glob import glob
cefpython_dir = os.path.dirname(cefpython3.__file__)

import gmail_desktop
gmail_desktop_dir = os.path.dirname(gmail_desktop.__file__)

added_files = [
         ( os.path.join(gmail_desktop_dir, 'gmail-icon.ico'), '.' ),
         ( os.path.join(cefpython_dir, '*.pak'), '.' ),
         ( os.path.join(cefpython_dir, '*.dat'), '.' ),
         ( os.path.join(cefpython_dir, '*.bin'), '.' ),
         ( os.path.join(cefpython_dir, '*.exe'), '.' ),
         ( os.path.join(cefpython_dir, '*.dll'), '.' ),
         ( os.path.join(cefpython_dir, 'locales'), 'locales' ),
         ]


a = Analysis(['gmail_desktop\\__init__.py'],
             pathex=['F:\\side\\gmail'],
             binaries=[],
             datas=added_files,
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='Gmail',
          debug=False,
          strip=False,
          upx=False,
          console=False , icon='gmail_desktop\\gmail-icon.ico')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=False,
               name='Gmail')
