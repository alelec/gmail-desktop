import os
import sys
import appdirs
import platform
import webbrowser
from gmail_desktop import win10toast
# from plyer import notification
from cefpython3 import cefpython as cef

# Fix for PyCharm hints warnings when using static methods
WindowUtils = cef.WindowUtils()

# Platforms
WINDOWS = (platform.system() == "Windows")
LINUX = (platform.system() == "Linux")
MAC = (platform.system() == "Darwin")

# Configuration
WIDTH = 1024
HEIGHT = 768

# Globals
g_count_windows = 0



from cefpython3 import cefpython as cef
import platform
import sys


def check_versions():
    print("[gmail] CEF Python {ver}".format(ver=cef.__version__))
    print("[gmail] Python {ver} {arch}".format(
          ver=platform.python_version(), arch=platform.architecture()[0]))
    assert cef.__version__ >= "55.3", "CEF Python v55.3+ required to run this"

def main():
    check_versions()

    appname = "Gmail"
    appauthor = "alelec"
    cache_path = appdirs.user_data_dir(appname, appauthor)
    print("Storing userdata in %s" % cache_path)

    sys.excepthook = cef.ExceptHook  # To shutdown all CEF processes on error
    settings = {"persist_user_preferences": True,
                "persist_session_cookies": True,
                "cache_path": cache_path}
    if WINDOWS:
        # High DPI support
        settings["auto_zooming"] = "system_dpi"
        # noinspection PyUnresolvedReferences, PyArgumentList
        cef.DpiAware.SetProcessDpiAware()  # Alternative is to embed manifest

    cef.Initialize(settings=settings)

    url = "https://mail.google.com"
    # url = "http://www.bennish.net/web-notifications.html"
    # url = "https://www.sitepoint.com/demos/html5desktopalerts/"
    # url = "http://demo.codeforgeek.com/notification/"
    browser = cef.CreateBrowserSync(url=url, window_title="Gmail",
                                    settings={'local_storage_disabled': False,
                                              'plugins_disabled': False})
    enable_notifications(browser)
    browser.SetClientHandler(LifespanHandler())

    cef.MessageLoop()
    cef.Shutdown()


def enable_notifications(browser):
    notification = Notification(browser)
    bindings = cef.JavascriptBindings(bindToFrames=True, bindToPopups=True)
    bindings.SetObject("Notification", notification, allow_properties=True)
    browser.SetJavascriptBindings(bindings)


class Notification(object):
    PERMISSION_DEFAULT = "default"
    PERMISSION_GRANTED = "granted"
    PERMISSION_DENIED = "denied"

    def __init__(self, browser):
        self.browser = browser
        self.notifications = win10toast.ToastNotifier(
            default_icon=os.path.join(os.path.dirname(__file__), "gmail-icon.ico"))

    def __call__(self, title, options):
        return self.createNotification(title, options)

    def createNotification(self, title, options):
        icon = options.get('icon')
        message = options.get('body', '')
        self.notifications.show_toast(title=title, msg=message, icon_path=icon)

    @property
    def permission(self):
        return self.PERMISSION_GRANTED

    def isSupported(self):
        return True

    def permissionLevel(self):
        return Notification.PERMISSION_GRANTED

    def requestPermission(self, callback):
        if callback:
            callback.Call(Notification.PERMISSION_GRANTED)


class LifespanHandler(object):
    def OnBeforePopup(self, browser, frame, target_url, target_frame_name, 
                      target_disposition, user_gesture, popup_features, 
                      window_info_out, client, browser_settings_out, 
                      no_javascript_access_out):
        cancel = False
        # print(target_url)
        if "mail.google.com" not in target_url:
            cancel = True
            webbrowser.open(target_url)
        return cancel


if __name__ == '__main__':
    main()
