from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

import os
__all__ = ['ToastNotifier']

# #############################################################################
# ########## Libraries #############
# ##################################
# standard library
import io
import tempfile
import logging
import requests
from os import path
from PIL import Image
from urllib.parse import urlsplit

# 3rd party modules
from win32api import GetModuleHandle
from win32api import PostQuitMessage
from win32con import CW_USEDEFAULT
from win32con import IDI_APPLICATION
from win32con import IMAGE_ICON
from win32con import LR_DEFAULTSIZE
from win32con import LR_LOADFROMFILE
from win32con import WM_DESTROY
from win32con import WM_USER
from win32con import WS_OVERLAPPED
from win32con import WS_SYSMENU
from win32gui import CreateWindow
from win32gui import DestroyWindow
from win32gui import LoadIcon
from win32gui import LoadImage
from win32gui import NIF_ICON
from win32gui import NIF_INFO
from win32gui import NIF_MESSAGE
from win32gui import NIF_TIP
from win32gui import NIM_ADD
from win32gui import NIM_DELETE
from win32gui import NIM_MODIFY
from win32gui import RegisterClass
from win32gui import Shell_NotifyIcon
from win32gui import UpdateWindow
from win32gui import WNDCLASS


TRAY_ICON_MESSAGE = WM_USER + 20

NOTIFYICON_VERSION_4 = 4

NIIF_USER = 4
NIIF_LARGE_ICON = 0x20

NIN_BALLOONSHOW = (WM_USER + 2)
NIN_BALLOONHIDE = (WM_USER + 3)
NIN_BALLOONTIMEOUT = (WM_USER + 4)
NIN_BALLOONUSERCLICK = (WM_USER + 5)


# ############################################################################
# ########### Classes ##############
# ##################################


class ToastNotifier(object):
    """Create a Windows 10  toast notification.
    from: https://github.com/jithurjacob/Windows-10-Toast-Notifications
    """
    NOTIFICATION_SHOW = "Show"
    NOTIFICATION_HIDE = "Hide"
    NOTIFICATION_TIMEOUT = "Timeout"
    NOTIFICATION_CLICK = "Click"
    NOTIFICATION_UNKNOWN = "Unknown"

    def __init__(self, tooltip="", default_icon=None):
        """Initialize."""
        message_map = {WM_DESTROY: self.on_destroy,
                       TRAY_ICON_MESSAGE: self.on_notification}

        self._id = id(self) & 0xFFFFFF
        self.tooltip = tooltip

        # Register the window class.
        wc = WNDCLASS()
        self.hinst = wc.hInstance = GetModuleHandle(None)
        wc.lpszClassName = str("PythonTaskbar")  # must be a string
        wc.lpfnWndProc = message_map  # could also specify a wndproc.
        self.classAtom = RegisterClass(wc)

        style = WS_OVERLAPPED | WS_SYSMENU
        self.hwnd = CreateWindow(self.classAtom, "Taskbar", style,
                                 0, 0, CW_USEDEFAULT,
                                 CW_USEDEFAULT,
                                 0, 0, self.hinst, None)
        UpdateWindow(self.hwnd)

        if default_icon:
            icon_flags = LR_LOADFROMFILE | LR_DEFAULTSIZE
            try:
                self.hicon = LoadImage(self.hinst, default_icon,
                                  IMAGE_ICON, 0, 0, icon_flags)
            except Exception as e:
                logging.error("Some trouble with the icon ({}): {}"
                              .format(default_icon, e))
                self.hicon = LoadIcon(0, IDI_APPLICATION)

        self.callback = None

    def download_icon(self, url):
        file_name = os.path.join(tempfile.gettempdir(), urlsplit(url)[2].split('/')[-1])
        r = requests.get(url, stream=True)
        if r.status_code == requests.codes.ok:
            with open(file_name, 'wb') as f:
                for chunk in r:
                    f.write(chunk)
        if not os.path.splitext(file_name)[-1].lower().endswith('ico'):
            img = Image.open(file_name)
            file_name = os.path.splitext(file_name)[0] + '.ico'
            img.save(file_name)
        return file_name

    def load_icon(self, icon_path):
        # icon
        hicon = self.hicon
        if icon_path is not None:
            try:
                if icon_path.startswith('http'):
                    icon_path = self.download_icon(icon_path)
                else:
                    icon_path = path.realpath(icon_path)
            except:
                logging.exception("Error loading icon")
                icon_path = None


        try:
            icon_flags = LR_LOADFROMFILE | LR_DEFAULTSIZE
            hicon = LoadImage(self.hinst, icon_path,
                              IMAGE_ICON, 0, 0, icon_flags)
        except Exception as e:
            logging.error("Some trouble with the icon ({}): {}"
                          .format(icon_path, e))

        return hicon

    def show_toast(self, title="Notification", msg="Here comes the message",
                    icon_path=None, callback=None):

        """Notification settings.
        :title: notification title
        :msg: notification message
        :icon_path: path to the .ico file to custom notification
        """

        self.callback = callback
        hicon = self.load_icon(icon_path)

        # Taskbar icon
        flags = NIF_ICON | NIF_MESSAGE | NIF_TIP | NIF_INFO
        Shell_NotifyIcon(NIM_ADD, (self.hwnd, self._id, flags, TRAY_ICON_MESSAGE,
                                   hicon, self.tooltip, msg, 200, title))
        Shell_NotifyIcon(NIM_DELETE, (self.hwnd, self._id))

    def on_notification(self, hwnd, msg, wparam, lparam):
        notification = ({NIN_BALLOONSHOW: self.NOTIFICATION_SHOW,
                         NIN_BALLOONHIDE: self.NOTIFICATION_HIDE,
                         NIN_BALLOONTIMEOUT: self.NOTIFICATION_TIMEOUT,
                         NIN_BALLOONUSERCLICK: self.NOTIFICATION_CLICK,
                         }.get(lparam, self.NOTIFICATION_UNKNOWN))
        if self.callback:
            self.callback(notification)

    def on_destroy(self, hwnd, msg, wparam, lparam):
        """Clean after notification ended.
        :hwnd:
        :msg:
        :wparam:
        :lparam:
        """
        Shell_NotifyIcon(NIM_DELETE, (self.hwnd, self._id))
        PostQuitMessage(0)

        return None
