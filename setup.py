import os
from setuptools import setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    long_description = readme.read()

setup(
    name='gmail_desktop',
    packages=['gmail_desktop'],
    include_package_data=True,
    description='desktop gmail client',
    long_description=long_description,
    author='Andrew Leech',
    author_email='andrew@alelec.net',
    url='https://gitlab.com/alelec/gmail-desktop',
    use_scm_version=True,
    setup_requires=['setuptools_scm', 'setuptools_git'],
    install_requires=['Pillow', 'requests', 'cefpython3', 'pypiwin32', 'appdirs'],
    entry_points={
        'gui_scripts': [
            'Gmail = gmail_desktop:main',
        ]
    }
)
